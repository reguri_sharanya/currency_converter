import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.applet.*;

class Converter extends Frame implements ActionListener,ItemListener
{
    JFrame frame;
    JTextField textfieldFrom,textfieldTo;
    JLabel l1,l2,l3,l4;
    JComboBox convertFrom,convertTo;
    JButton btnConvert,btnExit;
    JPanel mainPanel=new JPanel();
    
    double input=0;
    double result=0;
    
    public Converter()
    {
        super("Currency Converter");
        setSize(400,400);
        setLayout(null);
        setBackground(Color.GRAY);
        
        l1=new JLabel("From");
        l1.setBounds(30,45,250,30);
        l1.setForeground(Color.RED);
        add(l1);
        
        convertFrom=new JComboBox();
        convertFrom.setBounds(30,70,150,30);
        convertFrom.addItem("US Dollar");
        convertFrom.addItem("Japan Yen");
        convertFrom.addItem("Taiwan Dollar");
        convertFrom.addItem("Philippine Peso");
        convertFrom.addItem("Thai Baht");
        convertFrom.addItem("Rupees");
        add(convertFrom);
        
        l2=new JLabel("To");
        l2.setBounds(230,45,250,30);
        l2.setForeground(Color.RED);
        add(l2);
        
        convertTo=new JComboBox();
        convertTo.setBounds(230,70,150,30);
        convertTo.addItem("US Dollar");
        convertTo.addItem("Japan Yen");
        convertTo.addItem("Taiwan Dollar");
        convertTo.addItem("Philippine Peso");
        convertTo.addItem("Thai Baht");
        convertTo.addItem("Rupees");
        add(convertTo);
        
        l3=new JLabel("Enter amount to Convert:");
        l3.setBounds(50,110,300,30);
        add(l3);
        
        textfieldFrom=new JTextField(15);
        textfieldFrom.setBounds(50,140,300,30);
        add(textfieldFrom);
        
        btnConvert=new JButton("Convert");
        btnConvert.setBounds(100,250,100,30);
        add(btnConvert);
        
        l4=new JLabel("Total amount Converted:");
        l4.setBounds(50,170,300,30);
        add(l4);
        
        textfieldTo=new JTextField(15);
        textfieldTo.setBounds(50,200,300,30);
        textfieldTo.setEditable(false);
        textfieldTo.setForeground(Color.RED);
        add(textfieldTo);
        
        btnExit=new JButton("Exit");
        btnExit.setBounds(210,250,100,30);
        add(btnExit);
        
        convertFrom.addItemListener(this);              //this register all the GUI components who listen or invoke the   the methods
        convertTo.addItemListener(this);
        btnConvert.addActionListener(this);
        textfieldFrom.addActionListener(this);
        btnExit.addActionListener(this);
        
        addWindowListener(new WindowAdapter()                    //this will close the applet window if user clicks the close button
        {
            public void windowClosing(WindowEvent e)
            {
                exit();
            }
        });
    }
    public void exit()                                            //exit method
    {
        setVisible(false);
        dispose();
        System.exit(0);
    }
    
    public void ConvertDollar()
    {
        if(convertTo.getSelectedItem()=="Us Dollar")
        {
            result=(input);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Japan Yen")
        {
            result=(input*76.7);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Thaiwan Dollar")
        {
            result=(input*29.7);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Philippine Peso")
        {
            result=(input*43.8);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Rupees")
        {
            result=(input*73.93);
            textfieldTo.setText(""+result);
        }
        else
        {
            result=(input*31.8);
            textfieldTo.setText(""+result);
        }
    }
    public void ConvertRupee
    {
        if(convertTo.getSelectedItem()=="Us Dollar")
        {
            result=(input*0.014);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Japan Yen")
        {
            result=(input*1.55);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Thaiwan Dollar")
        {
            result=(input*0.37);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Philippine Peso")
        {
            result=(input*0.69);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Rupees")
        {
            result=(input);
            textfieldTo.setText(""+result);
        }
        else
        {
            result=(input*0.45);
            textfieldTo.setText(""+result);
        }
    }
    public void ConvertPeso()
    {
        if(convertTo.getSelectedItem()=="US Dollar")
        {
            result=(input*0.0228);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Philippine Peso")
        {
            result=(input);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Japan Yen")
        {
            result=(input*1.75);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Thai Bhat")
        {
            result=(input*0.722);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Rupees")
        {
            result=(input*1.44);
            textfieldTo.setText(""+result);
        }
        else
        {
            result=(input*0.681);
            textfieldTo.setText(""+result);
        }
    }
    public void ConvertYen() 
    {
        if(convertTo.getSelectedItem()=="US Dollar")
        {
            result = (input*0.013);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Philippine Peso")
        {
            result = (input*0.571);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Japan Yen")
        {
            result = (input);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Thai Baht")
        {
            result = (input*0.414);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Rupees")
        {
            result=(input*0.65);
            textfieldTo.setText(""+result);
        }
        else
        {
            result = (input*0.388);
            textfieldTo.setText(""+result);
        }
    }
    public void ConvertBaht()
    {
       
        if(convertTo.getSelectedItem()=="US Dollar")
        {
            result = (input * 0.0315);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Philippine Peso")
        {
            result = (input * 1.38);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Japan Yen")
        {
            result = (input * 2.42);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Thai Baht")
        {
            result = (input);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Rupees")
        {
            result=(input*2.23);
            textfieldTo.setText(""+result);
        }
        else
        {
            result = (input * 0.942);
            textfieldTo.setText(""+result);
        }
    }
     public void ConvertTaiDollar()
     {
        if(convertTo.getSelectedItem()=="US Dollar")
        {
            result = (input * 0.0336);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Philippine Peso")
        {
            result = (input * 1.47);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Japan Yen")
        {
            result = (input * 2.28);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Thai Baht")
        {
            result = (input * 1.06);
            textfieldTo.setText(""+result);
        }
        else if(convertTo.getSelectedItem()=="Rupees")
        {
            result=(input*2.67);
            textfieldTo.setText(""+result);
        }
        else
        {
            result = (input);
            textfieldTo.setText(""+result);
        }
    }
    public void actionPerformed(ActionEvent e)
    {
        input=Double.parseDouble(textfieldFrom.getText());
        String msg=textfieldFrom.getText();
        
        if(e.getSource()==btnConvert)
        {
            if(convertFrom.getSelectedItem()=="US Dollar")
            {
                ConvertDollar();
            }
            else if(convertFrom.getSelectedItem()=="Philippine Peso")
            {
                ConvertPeso();
            }
            else if(convertFrom.getSelectedItem()=="Japan Yen")
            {
                ConvertYen();
            }
            else if(convertFrom.getSelectedItem()=="Thai Baht")
            {
                ConvertBaht();
            }
            else if(convertFrom.getSelectedItem()=="Rupees")
            {
                ConvertRupee();
            }
            else
            {
                ConvertTaiDollar();
            }
        }
        else                                            //to display a dialog box to confirm(whether to exit or not)
        {
            int choice=JOptionPane.showConfirmDialog(null,"Do you really want to quit?","Exit",JOptionPane.YES_NO_OPTION);
            if(choice==0)
            {
                dispose();
                System.exit(0);
            }
        }
    }
    public void itemStateChanged(ItemEvent ie)
    {}
    public static void main(String args[])                        //to make the applet window frame visible
    {
        Converter c=new Converter();
        c.setVisible(true);
    }
}
    
    
    
                
    